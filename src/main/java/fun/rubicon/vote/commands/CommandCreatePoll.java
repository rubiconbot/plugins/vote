/*
 * RubiconBot - A open source Discord bot
 * Copyright (C) 2018  RubiconBot Dev Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package fun.rubicon.vote.commands;

import fun.rubicon.plugin.command.Command;
import fun.rubicon.plugin.command.CommandCategory;
import fun.rubicon.plugin.command.CommandEvent;
import fun.rubicon.plugin.command.Result;
import fun.rubicon.plugin.util.Colors;
import fun.rubicon.vote.core.PollManager;
import fun.rubicon.vote.entities.Poll;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.Permission;
import net.dv8tion.jda.core.entities.Channel;
import net.dv8tion.jda.core.entities.Message;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

public class CommandCreatePoll extends Command {

    private final PollManager pollManager;

    CommandCreatePoll(PollManager manager) {
        super(new String[] {"create", "add", "make"}, CommandCategory.UTILITY, "<Title>|<Option1>|<Option2>|...", "Create a poll");
        this.pollManager = manager;
    }

    @Override
    public Result execute(CommandEvent event, String[] args) {
        if(pollManager.pollExists(event.getGuild()))
            return send(error("Already exists!", "There is already a poll running on this guild."));
        List<String> content = Arrays.asList(event.getArgsAsString().replaceFirst("create", "").split("\\|"));
        if(content.size() < 3)
            return send(error("To less arguments!", "You have to specify at least three arguments."));
        String heading = content.get(0);
        List<String> answers = new ArrayList<>(content.subList(1, content.size()));
        if (!event.getGuild().getSelfMember().hasPermission((Channel) event.getChannel(), Permission.MESSAGE_ADD_REACTION))
            return send(error("Missing permissions!", "Missing permission message_reaction_add."));
        Message pollMessage = event.getChannel().sendMessage(new EmbedBuilder().setDescription("Creating").setColor(Colors.BLUE).build()).complete();
        HashMap<String, Integer> reactions = new HashMap<>();
        final AtomicInteger count = new AtomicInteger();
        ArrayList<String> toAddEmojis = new ArrayList<>(Arrays.asList(Poll.EMOTI));
        answers.forEach(a -> {
            reactions.put(toAddEmojis.get(0), count.get() + 1);
            toAddEmojis.remove(0);
            count.addAndGet(1);
        });

        Poll poll = Poll.createPoll(heading, answers, pollMessage, reactions, event.getMember());
        poll.save();
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                pollMessage.editMessage(poll.formatMessage().build()).complete();
                poll.getReactions().keySet().forEach(r -> pollMessage.addReaction(r).queue());
            }
        }, 500);
        return null;
    }
}
