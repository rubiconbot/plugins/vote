/*
 * RubiconBot - A open source Discord bot
 * Copyright (C) 2018  RubiconBot Dev Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package fun.rubicon.vote.commands;

import fun.rubicon.plugin.command.Command;
import fun.rubicon.plugin.command.CommandCategory;
import fun.rubicon.plugin.command.CommandEvent;
import fun.rubicon.plugin.command.Result;
import fun.rubicon.plugin.util.SafeMessage;
import fun.rubicon.vote.core.PollManager;
import fun.rubicon.vote.entities.Poll;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.TextChannel;

public class CommandStatsPoll extends Command {

    private final PollManager pollManager;

    CommandStatsPoll(PollManager pollManager) {
        super(new String[] {"stats", "info"}, CommandCategory.UTILITY, "", "Post the statistics of a ");
        this.pollManager = pollManager;
    }

    @Override
    public Result execute(CommandEvent event, String[] args) {
        if (!pollManager.pollExists(event.getGuild()))
            return send(error("No poll!", "There is actually no poll running on this guild."));
        Poll poll = pollManager.getPollByGuild(event.getGuild());
        Message statsMessage = SafeMessage.sendMessageBlocking((TextChannel) event.getChannel(), poll.formatMessage().build());
        poll.getReactions().keySet().forEach(unicode -> statsMessage.addReaction(unicode).queue());
        poll.getPollMessages().put(statsMessage.getIdLong(), statsMessage.getTextChannel().getIdLong());
        pollManager.replacePoll(poll, event.getGuild());
        return null;
    }
}
