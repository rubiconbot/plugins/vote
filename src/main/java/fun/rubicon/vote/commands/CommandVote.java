/*
 * RubiconBot - A open source Discord bot
 * Copyright (C) 2018  RubiconBot Dev Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package fun.rubicon.vote.commands;

import fun.rubicon.plugin.command.Command;
import fun.rubicon.plugin.command.CommandCategory;
import fun.rubicon.plugin.command.CommandEvent;
import fun.rubicon.plugin.command.Result;
import fun.rubicon.vote.core.PollManager;
import fun.rubicon.vote.entities.Poll;

public class CommandVote extends Command {

    private final PollManager pollManager;

    CommandVote(PollManager pollManager) {
        super(new String[] {"v", "vote"}, CommandCategory.UTILITY, "<voteId>", "Vote on a poll");
        this.pollManager = pollManager;
    }

    @Override
    public Result execute(CommandEvent event, String[] args) {
        if (!pollManager.pollExists(event.getGuild()))
            return send(error("No poll!", "There is actually no poll running on this guild."));
        Poll poll = pollManager.getPollByGuild(event.getGuild());
        int voteId;
        try {
            voteId = Integer.parseInt(args[1]);
        } catch (NumberFormatException e){
            return send(error("Unknown number", "You have to specify a valid number"));
        }
        if (poll.getVotes().containsKey(event.getAuthor().getIdLong()))
            return null;

        poll.vote(event.getMember(), voteId);
        pollManager.replacePoll(poll, event.getGuild());
        poll.updateMessages(event.getGuild());
        return null;
    }
}
