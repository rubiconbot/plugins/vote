package fun.rubicon.vote.core;

import fun.rubicon.plugin.io.db.Cassandra;
import fun.rubicon.vote.VotePlugin;
import fun.rubicon.vote.entities.Poll;
import lombok.Getter;
import lombok.extern.log4j.Log4j;
import net.dv8tion.jda.core.entities.Guild;

import java.util.HashMap;

/**
 * @author Michael Rittmeister / Schlaubi
 * @license GNU General Public License v3.0
 */

@Log4j
public class PollManager {

    @Getter
    private HashMap<Guild, Poll> pollCache = new HashMap<>();

    public synchronized void cachePolls() {
        new Thread(() -> Cassandra.getCassandra().getMappingManager().createAccessor(PollAccessor.class).getAll().all().forEach(poll -> {
            poll.init();
            Guild guild = VotePlugin.getInstance().getRubicon().getShardManager().getGuildById(poll.getGuildId());
            pollCache.put(guild, poll);
            log.debug(String.format("[PollCache] Cached poll for guild: %s(%s)", guild.getName(), guild.getId()));
        }), "PollLoadingThread").start();
    }




    public Poll getPollByGuild(Guild guild) {
        return pollCache.get(guild);
    }

    public boolean pollExists(Guild guild) {
        return pollCache.containsKey(guild);
    }

    public void replacePoll(Poll poll, Guild guild) {
        poll.save();
        pollCache.replace(guild, poll);
    }



}