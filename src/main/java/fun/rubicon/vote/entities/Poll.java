/*
 * RubiconBot - A open source Discord bot
 * Copyright (C) 2018  RubiconBot Dev Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package fun.rubicon.vote.entities;

import com.datastax.driver.mapping.annotations.Column;
import com.datastax.driver.mapping.annotations.PartitionKey;
import com.datastax.driver.mapping.annotations.Table;
import fun.rubicon.plugin.io.db.Cassandra;
import fun.rubicon.plugin.io.db.DatabaseEntity;
import fun.rubicon.plugin.util.Colors;
import fun.rubicon.vote.VotePlugin;
import lombok.Getter;
import lombok.extern.log4j.Log4j;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.Message;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;

@Log4j
@Table(name = "polls")
public class Poll extends DatabaseEntity<Poll> {

    public final static String[] EMOTI = ("\uD83C\uDF4F \uD83C\uDF4E \uD83C\uDF50 \uD83C\uDF4A \uD83C\uDF4B \uD83C\uDF4C \uD83C\uDF49 \uD83C\uDF47 \uD83C\uDF53 \uD83C\uDF48 \uD83C\uDF52 \uD83C\uDF51 \uD83C\uDF4D \uD83E\uDD5D " +
            "\uD83E\uDD51 \uD83C\uDF45 \uD83C\uDF46 \uD83E\uDD52 \uD83E\uDD55 \uD83C\uDF3D \uD83C\uDF36 \uD83E\uDD54 \uD83C\uDF60 \uD83C\uDF30 \uD83E\uDD5C \uD83C\uDF6F \uD83E\uDD50 \uD83C\uDF5E " +
            "\uD83E\uDD56 \uD83E\uDDC0 \uD83E\uDD5A \uD83C\uDF73 \uD83E\uDD53 \uD83E\uDD5E \uD83C\uDF64 \uD83C\uDF57 \uD83C\uDF56 \uD83C\uDF55 \uD83C\uDF2D \uD83C\uDF54 \uD83C\uDF5F \uD83E\uDD59 " +
            "\uD83C\uDF2E \uD83C\uDF2F \uD83E\uDD57 \uD83E\uDD58 \uD83C\uDF5D \uD83C\uDF5C \uD83C\uDF72 \uD83C\uDF65 \uD83C\uDF63 \uD83C\uDF71 \uD83C\uDF5B \uD83C\uDF5A \uD83C\uDF59 \uD83C\uDF58 " +
            "\uD83C\uDF62 \uD83C\uDF61 \uD83C\uDF67 \uD83C\uDF68 \uD83C\uDF66 \uD83C\uDF70 \uD83C\uDF82 \uD83C\uDF6E \uD83C\uDF6D \uD83C\uDF6C \uD83C\uDF6B \uD83C\uDF7F \uD83C\uDF69 \uD83C\uDF6A \uD83E\uDD5B " +
            "\uD83C\uDF75 \uD83C\uDF76 \uD83C\uDF7A \uD83C\uDF7B \uD83E\uDD42 \uD83C\uDF77 \uD83E\uDD43 \uD83C\uDF78 \uD83C\uDF79 \uD83C\uDF7E \uD83E\uDD44 \uD83C\uDF74 \uD83C\uDF7D").split(" ");

    /*The id of the poll creator*/
    @Getter
    @Column(name = "creator_id")
    private Long creatorId;
    /*The guild of the poll*/
    @Getter
    @Column(name = "guild_id")
    @PartitionKey
    private Long guildId;
    /*The heading of the poll*/
    @Getter
    private String heading;
    /*List with all answers*/
    @Getter
    private List<String> answers;
    /*Hashmap with all pollmsgs and their channels*/
    @Getter
    @Column(name = "poll_messages")
    private Map<Long, Long> pollMessages;
    /* Hashmap with count of voted for every option*/
    @Getter
    private Map<Long, Integer> votes;
    /*Hashmaps with all emotes and their vote options*/
    @Getter
    private Map<String, Integer> reactions;

    private Poll(Member creator, String heading, List<String> answers, Message message, Guild guild) {
        super(Poll.class, Cassandra.getCassandra());
        this.creatorId = creator.getUser().getIdLong();
        this.heading = heading;
        this.answers = answers;
        this.pollMessages = new HashMap<>();
        this.votes = new HashMap<>();
        this.reactions = new HashMap<>();
        this.guildId = guild.getIdLong();

        this.pollMessages.put(message.getIdLong(), message.getTextChannel().getIdLong());
    }

    private Poll(){
        super(Poll.class, Cassandra.getCassandra());
    }

    public static Poll createPoll(String heading, List<String> answers, Message message, HashMap<String, Integer> emotes, Member author) {
        Poll poll = new Poll(author, heading, answers, message, message.getGuild());
        emotes.forEach((s, i) -> poll.reactions.put(s, i));
        VotePlugin.getPollManager().getPollCache().put(message.getGuild(), poll);
        return poll;
    }

    public void updateMessages(Guild guild) {
        getPollMessages().forEach((m, c) -> {
            try {
                Message pollmessage = guild.getTextChannelById(c).getMessageById(m).complete();
                pollmessage.editMessage(formatMessage().build()).queue();
            } catch (Exception ignored) {
            }
        });
    }

    public EmbedBuilder formatMessage(){
        StringBuilder ansSTR = new StringBuilder();
        final AtomicInteger count = new AtomicInteger();

        this.getAnswers().forEach(s -> {
            long votescount = this.getVotes().keySet().stream().filter(k -> votes.get(k).equals(count.get() + 1)).count();
            ansSTR.append(EMOTI[count.get()]).append(" - ").append(count.get() + 1).append("  -  ").append(s).append("  -  Votes: `").append(votescount).append("` \n");
            count.addAndGet(1);
        });

        return new EmbedBuilder()
                .setAuthor(String.format("%s's poll - %s", getMember().getEffectiveName(), heading), getMember().getUser().getAvatarUrl())
                .setDescription(":pencil:   " + heading + "\n\n" + ansSTR.toString())
                .setFooter("React or use rc!vote v <id> to vote", null)
                .setColor(Colors.BLUE);

    }

    private Member getMember(){
        return VotePlugin.getInstance().getRubicon().getShardManager().getGuildById(guildId).getMemberById(creatorId);
    }

    public void vote(Member member, int voteId){
        votes.put(member.getUser().getIdLong(), voteId);
    }

    public void init(){
        if(this.votes == null)
            this.votes = new HashMap<>();
    }

    public void close(Guild guild){
        VotePlugin.getPollManager().getPollCache().remove(guild);
        delete();
    }

    @Override
    public void save() {
        save(this, null, null);
    }

    @Override
    public void save(Consumer<Poll> onSuccess) {
        save(this, onSuccess, null);
    }

    @Override
    public void save(Consumer<Poll> onSuccess, Consumer<Throwable> onError) {
        save(this, onSuccess, onError);
    }

    @Override
    public void delete() {
        delete(this, null, null);
    }

    @Override
    public void delete(Consumer<Poll> onSuccess) {
        delete(this, onSuccess, null);
    }

    @Override
    public void delete(Consumer<Poll> onSuccess, Consumer<Throwable> onError) {
        save(this, onSuccess, onError);
    }
}
