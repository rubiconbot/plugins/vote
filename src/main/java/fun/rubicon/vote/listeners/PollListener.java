/*
 * RubiconBot - A open source Discord bot
 * Copyright (C) 2018  RubiconBot Dev Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package fun.rubicon.vote.listeners;

import fun.rubicon.vote.VotePlugin;
import fun.rubicon.vote.core.PollManager;
import fun.rubicon.vote.entities.Poll;
import net.dv8tion.jda.core.Permission;
import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.events.message.MessageDeleteEvent;
import net.dv8tion.jda.core.events.message.guild.react.GuildMessageReactionRemoveEvent;
import net.dv8tion.jda.core.events.message.react.MessageReactionAddEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;

import java.util.Timer;
import java.util.TimerTask;

public class PollListener extends ListenerAdapter {

    private PollManager pollManager = VotePlugin.getPollManager();

    @Override
    public void onMessageReactionAdd(MessageReactionAddEvent event) {
        new Thread(() -> handleMessageReaction(event), "PollMessageReactHandler-" + event.getMessageId()).start();
    }

    @Override
    public void onMessageDelete(MessageDeleteEvent event) {
        new Thread(() -> handleMessageDeletion(event), "PollMessageDeleteHandler-" + event.getMessageId()).start();
    }
    @Override
    public void onGuildMessageReactionRemove(GuildMessageReactionRemoveEvent event) {
        new Thread(() -> handleReactionRemove(event), "PollReactionRemoveHandler-" + event.getMessageId()).start();
    }

    private void handleMessageReaction(MessageReactionAddEvent event) {
        Guild guild = event.getGuild();
        if (event.getUser().isBot() || !pollManager.pollExists(guild)) return;
        Poll poll = pollManager.getPollByGuild(guild);
        if (!poll.getPollMessages().containsKey(event.getMessageIdLong())) return;
        if (poll.getVotes().containsKey(event.getUser().getIdLong())) {
            if(!event.getGuild().getSelfMember().hasPermission(event.getTextChannel(), Permission.MESSAGE_MANAGE)) return;
            new Timer().schedule(new TimerTask() {
                @Override
                public void run() {
                    event.getReaction().removeReaction(event.getUser()).queue();
                }
            }, 1000);
            return;
        }
        String emoji = event.getReactionEmote().getName();
        if(event.getGuild().getSelfMember().hasPermission(event.getTextChannel(), Permission.MESSAGE_MANAGE))
            event.getReaction().removeReaction(event.getUser()).queue();
        poll.vote(event.getMember(), poll.getReactions().get(emoji));
        poll.updateMessages(event.getGuild());
        pollManager.replacePoll(poll, guild);
    }

    private void handleMessageDeletion(MessageDeleteEvent event) {
        if (!pollManager.pollExists(event.getGuild())) return;
        Poll poll = pollManager.getPollByGuild(event.getGuild());
        if (!poll.getPollMessages().containsKey(event.getMessageIdLong())) return;
        poll.getPollMessages().remove(event.getMessageIdLong());
        if(poll.getPollMessages().isEmpty())
            poll.close(event.getGuild());
        else
            pollManager.replacePoll(poll, event.getGuild());

    }

    private void handleReactionRemove(GuildMessageReactionRemoveEvent event){
        try {
            if (!pollManager.pollExists(event.getGuild())) return;
            Poll poll = pollManager.getPollByGuild(event.getGuild());
            if (!poll.getPollMessages().containsKey(event.getMessageIdLong())) return;
            if(poll.getReactions().containsKey(event.getReactionEmote().getName()))
                event.getChannel().getMessageById(event.getMessageId()).complete().addReaction(event.getReactionEmote().getName()).queue();
        } catch (Exception ignored) {}
    }
}
