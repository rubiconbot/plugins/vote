/*
 * RubiconBot - A open source Discord bot
 * Copyright (C) 2018  RubiconBot Dev Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package fun.rubicon.vote;

import fun.rubicon.plugin.Plugin;
import fun.rubicon.plugin.io.db.Cassandra;
import fun.rubicon.vote.commands.CommandPoll;
import fun.rubicon.vote.core.PollManager;
import fun.rubicon.vote.listeners.PollListener;
import lombok.Getter;

public class VotePlugin extends Plugin {

    @Getter
    private static VotePlugin instance;
    @Getter
    private static PollManager pollManager;

    @Override
    public void init() {
        instance = this;
        pollManager = new PollManager();
        registerCommand(new CommandPoll());
        registerListener(new PollListener());
    }

    @Override
    public void onEnable() {
        createPollsTable();
        pollManager.cachePolls();
    }

    private void createPollsTable() {
        Cassandra.getCassandra().getConnection().execute(
                "CREATE TABLE IF NOT EXISTS polls (" +
                        "guild_id bigint primary key," +
                        "creator_id bigint," +
                        "heading text," +
                        "answers list<text>," +
                        "poll_messages map<bigint, bigint>," +
                        "votes map<bigint, int>," +
                        "reactions map<text, int>" +
                        ")");
    }
}
